/*	Mobile menu
	==================================================	*/
$(function() {
    var	btn_mobile   =  $('.Mainmenu-mobile'),
        menu 	     =  $('.Mainmenu').find('ul');

    btn_mobile.on('click', function(e) {
        e.preventDefault();

        var el  =  $(this);

        el.toggleClass('nav-active');
        menu.toggleClass('open-menu');
/*$(".Mainmenu-link").click(function () {
    $(".Mainmenu-list").css('max-height', 0)
});*/
    });
}); /*  <-- */
/*  Slider Galery
    ==================================================  */
$(document).ready(function () {
    var time = setInterval(function(){SliderNextImage();}, 6000);
    $('#btnAnt').click(function () {
        clearInterval(time);
        var size = $('.Slider-container').find('.Slider-element').size();
        $('.Slider-container').find('.Slider-element').each(function (index, value) {
            if ($(this).hasClass('Slider-visible')) {
                $(this).slideUp();
                $(this).removeClass('Slider-visible');
                if (index == 0) {
                    $($('.Slider-container').find('.Slider-element').get(size - 1)).fadeIn();
                    $($('.Slider-container').find('.Slider-element').get(size - 1)).addClass('Slider-visible');
                    return false;
                }
                else {
                    $($('.Slider-container').find('.Slider-element').get(index - 1)).fadeIn();
                    $($('.Slider-container').find('.Slider-element').get(index - 1)).addClass('Slider-visible');
                    return false;
                };
            };
        });
    });
    $('#btnSig').click(function () {
        clearInterval(time);
        var size = $('.Slider-container').find('.Slider-element').size();
        $('.Slider-container').find('.Slider-element').each(function (index, value) {
            if ($(this).hasClass('Slider-visible')) {
                $(this).slideUp();
                $(this).removeClass('Slider-visible');
                if (index + 1 < size) {
                    $($('.Slider-container').find('.Slider-element').get(index + 1)).fadeIn();
                    $($('.Slider-container').find('.Slider-element').get(index + 1)).addClass('Slider-visible');
                    return false;
                }
                else {
                    $($('.Slider-container').find('.Slider-element').get(0)).fadeIn();
                    $($('.Slider-container').find('.Slider-element').get(0)).addClass('Slider-visible');
                    return false;
                };
            };
        });
    });
});

function SliderNextImage () {

var size = $('.Slider-container').find('.Slider-element').size();

$('.Slider-container').find('.Slider-element').each(function (index, value) {
        if ($(this).hasClass('Slider-visible')) {
            $(this).slideUp();
            $(this).removeClass('Slider-visible');

            /* --> Animacion de imagenes segundarias */
            $('.zoomIn').addClass('animated zoomIn');
            $('.fadeInLeft').addClass('animated fadeInLeft');
            $('.fadeInUpBig').addClass('animated fadeInUpBig');
            $('.zoomInLeft').addClass('animated zoomInLeft');
            $('.zoomInRight').addClass('animated zoomInRight');
            $('.rotateInUpRight').addClass('animated rotateInUpRight');

            if (index + 1 < size) {
                $($('.Slider-container').find('.Slider-element').get(index + 1)).fadeIn();

                /* <-- */
                $($('.Slider-container').find('.Slider-element').get(index + 1)).addClass('Slider-visible');
                return false;
            }
            else {
                $($('.Slider-container').find('.Slider-element').get(0)).fadeIn();
                $($('.Slider-container').find('.Slider-element').get(0)).addClass('Slider-visible');
                return false;
            };
        };
    });
};

$(function(){
    //clic en un enlace de la lista
    $('ul li a').on('click',function(e){
        //prevenir en comportamiento predeterminado del enlace
        e.preventDefault();
        //obtenemos el id del elemento en el que debemos posicionarnos
        var strAncla=$(this).attr('href');

        //utilizamos body y html, ya que dependiendo del navegador uno u otro no funciona
        $('body,html').stop(true,true).animate({
            //realizamos la animacion hacia el ancla
            scrollTop: $(strAncla).offset().top
        },1000);
    });
});